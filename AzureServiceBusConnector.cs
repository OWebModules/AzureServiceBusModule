﻿using AzureServiceBusModule.Models;
using AzureServiceBusModule.Services;
using AzureServiceBusModule.Validation;
using ElasticSearchNestConnector;
using FileResourceModule;
using LogModule;
using LogModule.Models;
using MediaViewerModule.Connectors;
using MediaViewerModule.Primitives;
using Microsoft.Azure.ServiceBus;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Converters;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureServiceBusModule
{
    public delegate void JobRequestHandler(AutomationEntity automationEntity);
    public delegate void QueueErrorHandler(ExceptionReceivedEventArgs eventArgs);
    public delegate void JsonErrorHandler(string message);
    public delegate void JobRequestCompletedHandler(AutomationEntity automationEntity);

    public class AzureServiceBusConnector : AppController
    {
        public event JobRequestHandler JobRequest;
        public event QueueErrorHandler QueueError;
        public event JsonErrorHandler JsonError;
        public event JobRequestCompletedHandler JobRequestCompleted;

        private QueueClient queueClient;

        public async Task<ResultItem<List<SendJobMessageResult>>> SendJobMessage(SendJobMessageRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<SendJobMessageResult>>>(async () =>
           {
               var result = new ResultItem<List<SendJobMessageResult>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<SendJobMessageResult>()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateSendJobMessageRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Validated request.");

               var serviceAgent = new ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo("Get Model...");
               var getModelResult = await serviceAgent.GetSendJobMessageModel(request);
               result.ResultState = getModelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have model.");

               request.MessageOutput?.OutputInfo($"Iterate through {getModelResult.Result.Configs.Count} Job-Request...");
               var connectionString = (from connectionConfigRel in getModelResult.Result.ConfigsToConnectionConfigs
                                       join connectionStringRel in getModelResult.Result.ConnectionConfigsToConnectionStrings on connectionConfigRel.ID_Other equals connectionStringRel.ID_Object
                                       join connectionStringAtt in getModelResult.Result.ConnectionStrings on connectionStringRel.ID_Other equals connectionStringAtt.ID_Object
                                       select connectionStringAtt.Val_String).FirstOrDefault();

               var queueName = (from connectionConfigRel in getModelResult.Result.ConfigsToConnectionConfigs
                                join queueRel in getModelResult.Result.ConnectionConfigToQueuesTopicsRelays on connectionConfigRel.ID_Other equals queueRel.ID_Object
                                select queueRel.Name_Other).FirstOrDefault();
               if (string.IsNullOrEmpty(connectionString))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No ConnectionString found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (string.IsNullOrEmpty(queueName))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No queueName found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }


               try
               {
                   if (queueClient == null || queueClient.IsClosedOrClosing)
                   {
                       queueClient = new QueueClient(connectionString, queueName);
                   }

                   foreach (var config in getModelResult.Result.Configs)
                   {
                       var resultItem = new SendJobMessageResult
                       {
                           Result = Globals.LState_Success.Clone(),
                           Config = config
                       };
                       result.Result.Add(resultItem);

                       request.MessageOutput?.OutputInfo($"Jobrequest {config.Name}");
                       var jobJson = (from configToStartSyncConfig in getModelResult.Result.ConfigsToSyncStartModuleConfigs.Where(rel => rel.ID_Object == config.GUID)
                                      join syncStarterJson in getModelResult.Result.StartModuleConfigsJsons on configToStartSyncConfig.ID_Other equals syncStarterJson.ID_Object
                                      select syncStarterJson.Val_String).FirstOrDefault();

                       var jobNamespace = (from configToStartSyncConfig in getModelResult.Result.ConfigsToSyncStartModuleConfigs.Where(rel => rel.ID_Object == config.GUID)
                                           join syncStarterNamespace in getModelResult.Result.StartModuleConfigsParameterNamespaces on configToStartSyncConfig.ID_Other equals syncStarterNamespace.ID_Object
                                           select syncStarterNamespace.Val_String).FirstOrDefault();

                       if (string.IsNullOrEmpty(jobJson))
                       {
                           resultItem.Result = Globals.LState_Error.Clone();
                           resultItem.Result.Additional1 = "The Job-Json is empty!";
                       }

                       var automationEntity = new AutomationEntity { JobJson = jobJson, ParameterNamespace = jobNamespace };
                       var automationEntityJson = Newtonsoft.Json.JsonConvert.SerializeObject(automationEntity);

                       var message = new Message(Encoding.UTF8.GetBytes(automationEntityJson));
                       await queueClient.SendAsync(message);
                   }

                   await queueClient.CloseAsync();
               }
               catch (Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = ex.Message;
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;

               }


               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<ReceiveJobMessageResult>> ReceiveJobMessage(ReceiveJobMessageRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ReceiveJobMessageResult>>(async () =>
           {
               var result = new ResultItem<ReceiveJobMessageResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new ReceiveJobMessageResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateReceiveJobMessageRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var serviceAgent = new ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo("Get Model...");

               var getModelResult = await serviceAgent.GetReceiveJobMessageModel(request);
               result.ResultState = getModelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have model.");

               var connectionString = getModelResult.Result.ConnectionString.Val_String;

               var queueName = getModelResult.Result.ConnectionConfigToQueuesTopicsRelay.Name_Other;

               if (string.IsNullOrEmpty(connectionString))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No ConnectionString found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (string.IsNullOrEmpty(queueName))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No queueName found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }


               try
               {
                   request.MessageOutput?.OutputInfo("Create client...");
                   if (queueClient == null || queueClient.IsClosedOrClosing)
                   {
                       queueClient = new QueueClient(connectionString, queueName);
                   }
                   request.MessageOutput?.OutputInfo("Have client.");

                   var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
                   {
                       MaxConcurrentCalls = 1,
                       AutoComplete = false
                   };

                   request.MessageOutput?.OutputInfo("Register Message Handler...");
                   queueClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);

                   JsonError += (errorMessage) =>
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = errorMessage;
                   };
                   request.MessageOutput?.OutputInfo("Registered Message Handler.");

                   request.MessageOutput?.OutputInfo("Receive messages...");
                   while (!request.CancellationToken.IsCancellationRequested)
                   {

                   }
                   request.MessageOutput?.OutputInfo("Stop receiving messages.");

                   await queueClient.CloseAsync();
               }
               catch (Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = ex.Message;
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;

               }

               return result;
           });

            return taskResult;
        }

        private async Task ProcessMessagesAsync(Message message, CancellationToken token)
        {
            var content = Encoding.UTF8.GetString(message.Body);
            try
            {
                var automationEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<AutomationEntity>(content);
                if (automationEntity == null)
                {
                    JsonError?.Invoke("No valid Json!");
                }
                JobRequest?.Invoke(automationEntity);
                await queueClient.CompleteAsync(message.SystemProperties.LockToken);
                JobRequestCompleted?.Invoke(automationEntity);


            }
            catch (Exception ex)
            {
                JsonError?.Invoke(ex.Message);
            }
        }

        private bool IsValidJson(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput)) { return false; }
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task<ResultItem<ArchiveFilesItemsResult>> ArchiveFiles(ArchiveFilesItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ArchiveFilesItemsResult>>(async () =>
           {
               var result = new ResultItem<ArchiveFilesItemsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new ArchiveFilesItemsResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateArchiveFilesRequest(request, Globals);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Validated request.");

               var mediaViewerController = new MediaViewerConnector(Globals);

               request.MessageOutput?.OutputInfo("Check Media-Service...");
               result.ResultState = await mediaViewerController.CheckMediaServier();
               if (result.ResultState.GUID  == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Checked Media-Service.");

               request.MessageOutput?.OutputInfo("Get model...");
               var serviceAgent = new Services.ServiceAgentElastic(Globals);
               var modelResult = await serviceAgent.GetArchiveFilesModel(request);
               result.ResultState = modelResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Have model.");


               request.MessageOutput?.OutputInfo("Get Elasticsearch-Log...");
               var logController = new LogController(Globals);
               var getElasticSearchLogRequest = new GetElasticSearchLogRequest(modelResult.Result.RootConfig.GUID)
               {
                   MessageOutput = request.MessageOutput
               };
               var elasticSearchLogResult = await logController.GetElasticSearchLog(getElasticSearchLogRequest);

               result.ResultState = elasticSearchLogResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var logDbReader = new clsUserAppDBSelector(elasticSearchLogResult.Result.EsServer, (int)elasticSearchLogResult.Result.EsPort, elasticSearchLogResult.Result.EsIndex, Globals.SearchRange, Globals.Session);
               var logDbWriter = new clsUserAppDBUpdater(logDbReader);

               var connectionString = modelResult.Result.AccountConfigsToConnectionStrings.OrderBy(conn => conn.OrderID).Select(conn => conn.Name_Other).First();
               var endpoint = modelResult.Result.AccountConfigsToUrls.Select(url => url.Name_Other).First();

               var storageAccount = CloudStorageAccount.Parse(connectionString);
               var client = storageAccount.CreateCloudBlobClient();

               var fileResourceController = new FileResourceController(Globals);
               foreach (var config in modelResult.Result.Configs)
               {
                   request.MessageOutput?.OutputInfo($"Config: {config.Name}");

                   var exportFolder = modelResult.Result.ConfigsToExportFolder.FirstOrDefault(exportFolder1 => exportFolder1.ID_Object == config.GUID);
                   var referenceItem = modelResult.Result.ConfigsToReferenceList.FirstOrDefault(refItem => refItem.ID_Object == config.GUID);
                   var mediaTypeRel = modelResult.Result.ConfigsToMediaTypes.FirstOrDefault(mediaType => mediaType.ID_Object == config.GUID);
                   var mediaTypeVal = MultimediaItemType.Image;

                   if (mediaTypeRel.ID_Other == AzureServiceBusModule.ArchiveFiles.Config.LocalData.Object_Audio.GUID)
                   {
                       mediaTypeVal = MultimediaItemType.Audio;
                   }
                   else if (mediaTypeRel.ID_Other == AzureServiceBusModule.ArchiveFiles.Config.LocalData.Object_Pdf.GUID)
                   {
                       mediaTypeVal = MultimediaItemType.PDF;
                   }
                   else if (mediaTypeRel.ID_Other == AzureServiceBusModule.ArchiveFiles.Config.LocalData.Object_Video.GUID)
                   {
                       mediaTypeVal = MultimediaItemType.Video;
                   }
                   else if (mediaTypeRel.ID_Other == AzureServiceBusModule.ArchiveFiles.Config.LocalData.Object_Other.GUID)
                   {
                       mediaTypeVal = MultimediaItemType.Unknown;
                   }

                   if (referenceItem != null)
                   {
                       if (referenceItem.ID_Parent_Other == AzureServiceBusModule.ArchiveFiles.Config.LocalData.Class_Fileresource.GUID)
                       {
                           result = await ArchiveFileResource(client,
                               fileResourceController,
                               new clsOntologyItem
                               {
                                   GUID = referenceItem.ID_Other,
                                   Name = referenceItem.Name_Other,
                                   GUID_Parent = referenceItem.ID_Parent_Other,
                                   Type = referenceItem.Ontology
                               },
                               exportFolder?.Name_Other,
                               request.MessageOutput,
                               logDbWriter,
                               elasticSearchLogResult.Result,
                               config.GUID);
                       }
                       else
                       {
                           

                           result = await ArchiveMediaItems(mediaViewerController, new clsOntologyItem
                           {
                               GUID = referenceItem.ID_Other,
                               Name = referenceItem.Name_Other,
                               GUID_Parent = referenceItem.ID_Parent_Other,
                               Type = referenceItem.Ontology
                           }, 
                           mediaTypeVal, 
                           request.MessageOutput, 
                           config.Name, 
                           exportFolder?.Name_Other,
                           client,
                           logDbWriter,
                           elasticSearchLogResult.Result,
                           config.GUID);
                       }
                       
                   }
                   else
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "No media-List found!";
                       return result;
                   }
               }

               return result;
           });

            return taskResult;
        }

        private async Task<ResultItem<ArchiveFilesItemsResult>> ArchiveFileResource(CloudBlobClient client, 
            FileResourceController fileResourceController, 
            clsOntologyItem fileResource, 
            string exportFolder,
            IMessageOutput messageOutput,
            clsUserAppDBUpdater dbWriter,
            ElasticsearchLog elasticSearchLog,
            string idConfig)
        {
            var taskResult = await Task.Run<ResultItem<ArchiveFilesItemsResult>>(async () =>
            {
                var result = new ResultItem<ArchiveFilesItemsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ArchiveFilesItemsResult()
                };

                var fileResourceResult = await fileResourceController.GetFileResource(fileResource);
                result.ResultState = fileResourceResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (fileResourceResult.Result == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Fileresource cannot be found!";
                    messageOutput.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var filesResult = await fileResourceController.GetFilesByFileResource(fileResourceResult.Result);
                result.ResultState = filesResult.ResultState;
                CloudBlobContainer blobContainer = null;
                if (!string.IsNullOrEmpty(exportFolder))
                {
                    blobContainer = client.GetContainerReference(exportFolder.ToLower());
                    var exists = await blobContainer.ExistsAsync();

                    if (!exists)
                    {
                        await blobContainer.CreateAsync();
                    }

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        messageOutput.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }
                var documents = new List<clsAppDocuments>();
                foreach (var filePath in filesResult.Result)
                {
                    var id = MD5Converters.CalculateMD5Hash(idConfig + filePath);
                    var doc = new clsAppDocuments
                    {
                        Id = id,
                        Dict = new Dictionary<string, object>()
                    };
                    documents.Add(doc);

                    doc.Dict.Add(Primitives.FieldPath, filePath);

                    var paths = filePath.Split('\\').Where(pathPart => !string.IsNullOrEmpty(pathPart)).Select(pathPart => pathPart.ToLower()).ToList();
                    if (paths.Count > 1)
                    {
                        var rootPath = paths[0];

                        CloudBlobDirectory directory;
                        if (blobContainer != null)
                        {
                            directory = blobContainer.GetDirectoryReference(rootPath);
                        }
                        else
                        {
                            blobContainer = client.GetContainerReference(exportFolder ?? rootPath);
                            directory = blobContainer.GetDirectoryReference(rootPath);
                        }

                        for (int i = 1; i < paths.Count - 1; i++)
                        {
                            var path = paths[i].ToLower();
                            directory = directory.GetDirectoryReference(path);
                        }

                        var blob = directory.GetBlockBlobReference(paths.Last());
                        doc.Dict.Add(Primitives.FiledUrl, blob.Uri.AbsoluteUri);
                        try
                        {
                            await blob.UploadFromFileAsync(filePath);
                        }
                        catch (Exception ex)
                        {
                            result.ResultState = Globals.LState_Error.Clone();
                            result.ResultState.Additional1 = ex.Message;
                            return result;
                        }
                    }

                    if (documents.Count > elasticSearchLog.ItemsCountBeforeLogging)
                    {
                        result.ResultState = dbWriter.SaveDoc(documents, elasticSearchLog.EsType);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the ElasticSearch documents!";
                            messageOutput.OutputError(result.ResultState.Additional1);
                            return result;

                        }
                        documents.Clear();
                    }
                    
                }

                if (documents.Count > 0)
                {
                    result.ResultState = dbWriter.SaveDoc(documents, elasticSearchLog.EsType);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the ElasticSearch documents!";
                        messageOutput.OutputError(result.ResultState.Additional1);
                        return result;

                    }
                    documents.Clear();
                }

                return result;
            });

            return taskResult;
        }

        private async Task<ResultItem<ArchiveFilesItemsResult>> ArchiveMediaItems(MediaViewerConnector mediaViewerController, 
            clsOntologyItem referenceItem, 
            MultimediaItemType mediaTypeVal, 
            IMessageOutput messageOutput, 
            string configurationName, 
            string exportFolder,
            CloudBlobClient client,
            clsUserAppDBUpdater dbWriter,
            ElasticsearchLog elasticSearchLog,
            string idConfig)
        {
            var taskResult = await Task.Run<ResultItem<ArchiveFilesItemsResult>>(async () =>
            {
                var result = new ResultItem<ArchiveFilesItemsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ArchiveFilesItemsResult()
                };

                var tempPath = $@"%TEMP%\{Guid.NewGuid().ToString()}";
                if (Directory.Exists(tempPath))
                {
                    Directory.Delete(tempPath, true);
                }

                var mediaItems = await mediaViewerController.GetMediaListItems(new List<OntologyClasses.BaseClasses.clsOntologyItem>
                       {
                           referenceItem
                       }, tempPath, null, mediaTypeVal, messageOutput);
                result.ResultState = mediaItems.Result;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var folderName = exportFolder == null ? configurationName.ToLower() : exportFolder.ToLower();
                var blobContainer = client.GetContainerReference(folderName);
                var exists = await blobContainer.ExistsAsync();

                if (!exists)
                {
                    await blobContainer.CreateAsync();
                }
                tempPath = Environment.ExpandEnvironmentVariables(tempPath);
                var documents = new List<clsAppDocuments>();
                foreach (var mediaItem in mediaItems.MediaListItems)
                {
                    var id = mediaItem.IdMultimediaItem;
                    var doc = new clsAppDocuments
                    {
                        Id = id,
                        Dict = new Dictionary<string, object>()
                    };
                    documents.Add(doc);

                    var path = "";
                    if (tempPath.EndsWith("\\"))
                    {
                        path = $"{tempPath}{mediaItem.source.Replace("/", "\\")}";
                    }
                    else
                    {
                        path = $@"{tempPath}\{mediaItem.source.Replace("/", "\\")}";
                    }

                    var blob = blobContainer.GetBlockBlobReference(mediaItem.NameMultimediaItem);
                    try
                    {
                        doc.Dict.Add(Primitives.FiledUrl, blob.Uri.AbsoluteUri);
                        await blob.UploadFromFileAsync(path);
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = ex.Message;
                        return result;
                    }

                    if (documents.Count > elasticSearchLog.ItemsCountBeforeLogging)
                    {
                        result.ResultState = dbWriter.SaveDoc(documents, elasticSearchLog.EsType);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the ElasticSearch documents!";
                            messageOutput.OutputError(result.ResultState.Additional1);
                            return result;

                        }
                        documents.Clear();
                    }
                }

                if (documents.Count > 0)
                {
                    result.ResultState = dbWriter.SaveDoc(documents, elasticSearchLog.EsType);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the ElasticSearch documents!";
                        messageOutput.OutputError(result.ResultState.Additional1);
                        return result;

                    }
                    documents.Clear();
                }

                return result;
            });

            return taskResult;
        }

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            QueueError?.Invoke(exceptionReceivedEventArgs);
            return Task.CompletedTask;
        }


        public AzureServiceBusConnector(Globals globals) : base(globals)
        {
        }
    }
}
