﻿using AzureServiceBusModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateSendJobMessageRequest(SendJobMessageRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( request.IdConfig ))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateReceiveJobMessageRequest(ReceiveJobMessageRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateSendJobMessageModel(SendJobMessageModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.RootConfig))
            {
                if (model.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The RootConfig is Empty!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.ConfigsToSyncStartModuleConfigs))
            {
                if (model.Configs.Count != model.ConfigsToSyncStartModuleConfigs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There are {model.Configs.Count} Job-Messages, but {model.ConfigsToSyncStartModuleConfigs.Count} SyncStarter-Configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.ConfigsToConnectionConfigs))
            {
                if (model.ConfigsToConnectionConfigs.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There must be one connection-config, but there are {model.ConfigsToConnectionConfigs.Count} connection-configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.ConnectionConfigsToConnectionStrings))
            {
                if (model.ConnectionConfigsToConnectionStrings.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There must be one connection-string, but there are {model.ConnectionConfigsToConnectionStrings.Count} connection-strings!";
                    return result;
                }
            }


            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.ConnectionStrings))
            {
                if (model.ConnectionConfigsToConnectionStrings.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There must be one connection-string, but there are {model.ConnectionStrings.Count} connection-strings!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.ConnectionConfigToQueuesTopicsRelays))
            {
                if (model.ConnectionConfigsToConnectionStrings.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There must be one queue, topic or relay, but there are {model.ConnectionConfigToQueuesTopicsRelays.Count} queues, topics or relays!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.StartModuleConfigsJsons))
            {
                if (model.StartModuleConfigsJsons.Count != model.ConfigsToSyncStartModuleConfigs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There must be {model.ConfigsToSyncStartModuleConfigs.Count} Job-Jsons, but there are only {model.StartModuleConfigsJsons.Count} Job-Jsons!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(SendJobMessageModel.StartModuleConfigsParameterNamespaces))
            {
                if (model.StartModuleConfigsParameterNamespaces.Count != model.ConfigsToSyncStartModuleConfigs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There must be {model.ConfigsToSyncStartModuleConfigs.Count} Job-Jsons, but there are only {model.StartModuleConfigsParameterNamespaces.Count} Job-Jsons!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateReceiveJobMessageModel(ReceiveJobMessageModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReceiveJobMessageModel.Config))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Config is Empty!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReceiveJobMessageModel.ConfigToConnectionConfig))
            {
                if (model.ConfigToConnectionConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"No connection-config found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReceiveJobMessageModel.ConnectionConfigToConnectionString))
            {
                if (model.ConnectionConfigToConnectionString == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"No Connection-String found!";
                    return result;
                }
            }


            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReceiveJobMessageModel.ConnectionString))
            {
                if (model.ConnectionString == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"No Connection-Config found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ReceiveJobMessageModel.ConnectionConfigToQueuesTopicsRelay))
            {
                if (model.ConnectionConfigToConnectionString == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There is no Queue found!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateArchiveFilesRequest(ArchiveFilesItemsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config-Id is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetArchiveFilesModel(GetArchiveFilesItemsModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetArchiveFilesItemsModel.RootConfig))
            {
                if (model.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Rootconfig is empty!";
                    return result;
                }

                if (model.RootConfig.GUID_Parent != ArchiveFiles.Config.LocalData.Class_Archive_Files.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Root-Config is not valid!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetArchiveFilesItemsModel.ConfigsToExportFolder))
            {
                if (model.ConfigsToExportFolder.Count > model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"There are more export-folders defined as configurations:{model.Configs} configs / {model.ConfigsToExportFolder.Count} folders";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetArchiveFilesItemsModel.ConfigsToAccountConfig))
            {
                if (model.ConfigsToAccountConfig.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Only one Storage Account Config is allowed. {model.ConfigsToAccountConfig.Count} are provided!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetArchiveFilesItemsModel.AccountConfigsToConnectionStrings))
            {
                if (model.AccountConfigsToConnectionStrings.Count != 2)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You must provide two connection-strings! {model.AccountConfigsToConnectionStrings.Count} are provided!";
                    return result;
                }

                if (model.AccountConfigsToConnectionStrings.Where(conn => conn.OrderID ==1).Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You must provide one connection-string with OrderId 1";
                    return result;
                }

                if (model.AccountConfigsToConnectionStrings.Where(conn => conn.OrderID == 2).Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You must provide one connection-string with OrderId 2";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetArchiveFilesItemsModel.AccountConfigsToUrls))
            {
                if (model.AccountConfigsToUrls.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You must provide one Endpoint-Url!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetArchiveFilesItemsModel.ConfigsToMediaTypes))
            {
                if (model.ConfigsToMediaTypes.Count !=  model.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You must provide a media-type per config. Configs: {model.Configs.Count}, Media-Types: {model.ConfigsToMediaTypes.Count}!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetArchiveFilesItemsModel.ConfigsToReferenceList))
            {
                if (!model.ConfigsToReferenceList.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"No Media-List provided!";
                    return result;
                }
            }

            return result;
        }
    }
}
