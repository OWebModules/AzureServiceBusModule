﻿using AzureServiceBusModule.Models;
using AzureServiceBusModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {
        public async Task<ResultItem<ReceiveJobMessageModel>> GetReceiveJobMessageModel(ReceiveJobMessageRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ReceiveJobMessageModel>>(() =>
           {
               var result = new ResultItem<ReceiveJobMessageModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ReceiveJobMessageModel()
               };

               result.ResultState = ValidationController.ValidateReceiveJobMessageRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = Config.LocalData.Class_Receive_Job_Message.GUID
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Config!";
                   return result;
               }

               result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateReceiveJobMessageModel(result.Result, globals, nameof(ReceiveJobMessageModel.Config));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConfigsToConnectionConfig = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Receive_Job_Message_Config_Azure_Service_Bus_Module.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Receive_Job_Message_Config_Azure_Service_Bus_Module.ID_Class_Right
                   }
               };


               var dbReaderConfigsToConnectionConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigsToConnectionConfig.GetDataObjectRel(searchConfigsToConnectionConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between JobConfig and ConnectionConfig!";
                   return result;
               }

               result.Result.ConfigToConnectionConfig = dbReaderConfigsToConnectionConfig.ObjectRels.FirstOrDefault();

               result.ResultState = ValidationController.ValidateReceiveJobMessageModel(result.Result, globals, nameof(ReceiveJobMessageModel.ConfigToConnectionConfig));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConnectionConfigToConnectionStrings = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigToConnectionConfig.ID_Other,
                       ID_RelationType = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Connection_String__Service_Bus_.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Connection_String__Service_Bus_.ID_Class_Right
                   }
               };
               
               var dbReaderConnectionConfigToConnectionStrings = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConnectionConfigToConnectionStrings.GetDataObjectRel(searchConnectionConfigToConnectionStrings);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between the ConnectionConfig and the ConnectionString!";
                   return result;
               }

               result.Result.ConnectionConfigToConnectionString = dbReaderConnectionConfigToConnectionStrings.ObjectRels.FirstOrDefault();

               result.ResultState = ValidationController.ValidateReceiveJobMessageModel(result.Result, globals, nameof(ReceiveJobMessageModel.ConnectionConfigToConnectionString));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConnectionStrings = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.ConnectionConfigToConnectionString.ID_Other,
                       ID_AttributeType = Config.LocalData.ClassAtt_Connection_String__Service_Bus__Content.ID_AttributeType
                   }
               };

               var dbReaderConnectionStrings = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConnectionStrings.GetDataObjectAtt(searchConnectionStrings);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Connection-String!";
                   return result;
               }

               result.Result.ConnectionString = dbReaderConnectionStrings.ObjAtts.FirstOrDefault();

               result.ResultState = ValidationController.ValidateReceiveJobMessageModel(result.Result, globals, nameof(ReceiveJobMessageModel.ConnectionString));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchQueuesTopicsOrRelays = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigToConnectionConfig.ID_Other,
                       ID_RelationType = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Queue__Azure_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Queue__Azure_.ID_Class_Right
                   }
               };


               var dbReaderQueuesTopicsOrRelays = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderQueuesTopicsOrRelays.GetDataObjectRel(searchQueuesTopicsOrRelays);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Queues, Topics or Relays!";
                   return result;
               }

               result.Result.ConnectionConfigToQueuesTopicsRelay = dbReaderQueuesTopicsOrRelays.ObjectRels.FirstOrDefault();

               result.ResultState = ValidationController.ValidateReceiveJobMessageModel(result.Result, globals, nameof(ReceiveJobMessageModel.ConnectionConfigToQueuesTopicsRelay));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.ConnectionConfigToQueuesTopicsRelay = dbReaderQueuesTopicsOrRelays.ObjectRels.FirstOrDefault();

               result.ResultState = ValidationController.ValidateReceiveJobMessageModel(result.Result, globals, nameof(ReceiveJobMessageModel.ConnectionConfigToQueuesTopicsRelay));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }


               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<SendJobMessageModel>> GetSendJobMessageModel(SendJobMessageRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SendJobMessageModel>>(() =>
           {
               var result = new ResultItem<SendJobMessageModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new SendJobMessageModel()
               };

               result.ResultState = ValidationController.ValidateSendJobMessageRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = Config.LocalData.Class_Send_Job_Message.GUID
                   }
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Root-Config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.RootConfig));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Send_Job_Message_contains_Send_Job_Message.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Send_Job_Message_contains_Send_Job_Message.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between the Job-Message config and the Connection-config!";
                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);
               }

               var searchConfigsToSyncStarterConfig = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Send_Job_Message_Content_ModuleConfig__SyncStarter_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Send_Job_Message_Content_ModuleConfig__SyncStarter_.ID_Class_Right
               }).ToList();

               var dbReaderConfigsToSyncStarterConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigsToSyncStarterConfig.GetDataObjectRel(searchConfigsToSyncStarterConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between Job-Config and Sync-Starter Config!";
                   return result;
               }

               result.Result.ConfigsToSyncStartModuleConfigs = dbReaderConfigsToSyncStarterConfig.ObjectRels;

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.ConfigsToSyncStartModuleConfigs));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchJobConfigsToConnectionConfigs = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Send_Job_Message_Config_Azure_Service_Bus_Module.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Send_Job_Message_Config_Azure_Service_Bus_Module.ID_Class_Right
               }).ToList();

               var dbReaderJobConfigsToConnectionConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderJobConfigsToConnectionConfigs.GetDataObjectRel(searchJobConfigsToConnectionConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between JobConfig and ConnectionConfig!";
                   return result;
               }

               result.Result.ConfigsToConnectionConfigs = dbReaderJobConfigsToConnectionConfigs.ObjectRels;

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.ConfigsToConnectionConfigs));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConnectionConfigToConnectionStrings = result.Result.ConfigsToConnectionConfigs.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Connection_String__Service_Bus_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Connection_String__Service_Bus_.ID_Class_Right
               }).ToList();

               var dbReaderConnectionConfigToConnectionStrings = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConnectionConfigToConnectionStrings.GetDataObjectRel(searchConnectionConfigToConnectionStrings);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between the ConnectionConfig and the ConnectionString!";
                   return result;
               }

               result.Result.ConnectionConfigsToConnectionStrings = dbReaderConnectionConfigToConnectionStrings.ObjectRels;

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.ConnectionConfigsToConnectionStrings));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConnectionStrings = result.Result.ConnectionConfigsToConnectionStrings.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Other,
                   ID_AttributeType = Config.LocalData.ClassAtt_Connection_String__Service_Bus__Content.ID_AttributeType
               }).ToList();

               var dbReaderConnectionStrings = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConnectionStrings.GetDataObjectAtt(searchConnectionStrings);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Connection-String!";
                   return result;
               }

               result.Result.ConnectionStrings = dbReaderConnectionStrings.ObjAtts;

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.ConnectionStrings));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchQueuesTopicsOrRelays = result.Result.ConfigsToConnectionConfigs.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Queue__Azure_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Azure_Service_Bus_Module_uses_Queue__Azure_.ID_Class_Right
               }).ToList();

               var dbReaderQueuesTopicsOrRelays = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderQueuesTopicsOrRelays.GetDataObjectRel(searchQueuesTopicsOrRelays);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Queues, Topics or Relays!";
                   return result;
               }

               result.Result.ConnectionConfigToQueuesTopicsRelays = dbReaderQueuesTopicsOrRelays.ObjectRels;

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.ConnectionConfigToQueuesTopicsRelays));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchJobJson = result.Result.ConfigsToSyncStartModuleConfigs.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Other,
                   ID_AttributeType = SyncStarterModule.Config.LocalData.AttributeType_Json.GUID
               }).ToList();

               var dbReaderJobJson = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderJobJson.GetDataObjectAtt(searchJobJson);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Job-Json!";
                   return result;
               }

               result.Result.StartModuleConfigsJsons = dbReaderJobJson.ObjAtts;

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.StartModuleConfigsJsons));


               var searchJobParameterNamespace = result.Result.ConfigsToSyncStartModuleConfigs.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Other,
                   ID_AttributeType = SyncStarterModule.Config.LocalData.AttributeType_Parameter_Namespace.GUID
               }).ToList();

               var dbReaderJobParameterNamespace = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderJobParameterNamespace.GetDataObjectAtt(searchJobParameterNamespace);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Parameternamespace!";
                   return result;
               }

               result.Result.StartModuleConfigsParameterNamespaces = dbReaderJobParameterNamespace.ObjAtts;

               result.ResultState = ValidationController.ValidateSendJobMessageModel(result.Result, globals, nameof(SendJobMessageModel.StartModuleConfigsParameterNamespaces));

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<GetArchiveFilesItemsModel>> GetArchiveFilesModel(ArchiveFilesItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetArchiveFilesItemsModel>>(() =>
           {
               var result = new ResultItem<GetArchiveFilesItemsModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetArchiveFilesItemsModel()
               };

               result.ResultState = ValidationController.ValidateArchiveFilesRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Root-Config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetArchiveFilesModel(result.Result, globals, nameof(Models.GetArchiveFilesItemsModel.RootConfig));
               
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_contains_Archive_Files.ID_RelationType,
                       ID_Parent_Other = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_contains_Archive_Files.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Sub-Configs!";
                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);   
               }

               var searchExportFolder = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_export_to_Folder.ID_RelationType,
                   ID_Parent_Other = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_export_to_Folder.ID_Class_Right
               }).ToList();

               var dbReaderExportFolders = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderExportFolders.GetDataObjectRel(searchExportFolder);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Export-Folders!";
                   return result;
               }

               result.Result.ConfigsToExportFolder = dbReaderExportFolders.ObjectRels;

               var searchStorageAccountConfig = new List<clsObjectRel>
               {
                   new clsObjectRel
                    {
                        ID_Object = result.Result.RootConfig.GUID,
                        ID_RelationType = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_uses_Azure_Storage_Account.ID_RelationType,
                        ID_Parent_Other = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_uses_Azure_Storage_Account.ID_Class_Right
                    }
               };

               var dbReaderStorageAccountConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderStorageAccountConfig.GetDataObjectRel(searchStorageAccountConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Storage Account Config!";
                   return result;
               }

               result.Result.ConfigsToAccountConfig = dbReaderStorageAccountConfig.ObjectRels;

               result.ResultState = ValidationController.ValidateGetArchiveFilesModel(result.Result, globals, nameof(Models.GetArchiveFilesItemsModel.ConfigsToAccountConfig));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConnectionStrings = result.Result.ConfigsToAccountConfig.Select(accountConfigRel => new clsObjectRel
               {
                   ID_Object = accountConfigRel.ID_Other,
                   ID_RelationType = ArchiveFiles.Config.LocalData.ClassRel_Azure_Storage_Account_uses_Connection_String__Azure_Storage_Account_.ID_RelationType,
                   ID_Parent_Other = ArchiveFiles.Config.LocalData.ClassRel_Azure_Storage_Account_uses_Connection_String__Azure_Storage_Account_.ID_Class_Right
               }).ToList();

               var dbReaderConnectionStrings = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConnectionStrings.GetDataObjectRel(searchConnectionStrings);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Connection-Strings of Azure Storage Account!";
                   return result;
               }

               result.Result.AccountConfigsToConnectionStrings = dbReaderConnectionStrings.ObjectRels;

               result.ResultState = ValidationController.ValidateGetArchiveFilesModel(result.Result, globals, nameof(Models.GetArchiveFilesItemsModel.AccountConfigsToConnectionStrings));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchEndpointUrl = result.Result.ConfigsToAccountConfig.Select(accountConfigRel => new clsObjectRel
               {
                   ID_Object = accountConfigRel.ID_Other,
                   ID_RelationType = ArchiveFiles.Config.LocalData.ClassRel_Azure_Storage_Account_belonging_Source_Url.ID_RelationType,
                   ID_Parent_Other = ArchiveFiles.Config.LocalData.ClassRel_Azure_Storage_Account_belonging_Source_Url.ID_Class_Right
               }).ToList();

               var dbReaderEndpointUrl = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderEndpointUrl.GetDataObjectRel(searchEndpointUrl);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Endpoint-Urls of Azure Storage Account!";
                   return result;
               }
               result.Result.AccountConfigsToUrls = dbReaderEndpointUrl.ObjectRels;

               result.ResultState = ValidationController.ValidateGetArchiveFilesModel(result.Result, globals, nameof(Models.GetArchiveFilesItemsModel.AccountConfigsToUrls));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchMediaTypes = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_belonging_Media_Type.ID_RelationType,
                   ID_Parent_Other = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_belonging_Media_Type.ID_Class_Right
               }).ToList();

               var dbReaderMediaTypes = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderMediaTypes.GetDataObjectRel(searchMediaTypes);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Media-Types!";
                   return result;
               }

               result.Result.ConfigsToMediaTypes = dbReaderMediaTypes.ObjectRels;

               result.ResultState = ValidationController.ValidateGetArchiveFilesModel(result.Result, globals, nameof(Models.GetArchiveFilesItemsModel.ConfigsToMediaTypes));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchLists = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ArchiveFiles.Config.LocalData.ClassRel_Archive_Files_belonging_list.ID_RelationType
               }).ToList();

               var dbReaderLists = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderLists.GetDataObjectRel(searchLists);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Media-Lists!";
                   return result;
               }

               result.Result.ConfigsToReferenceList = dbReaderLists.ObjectRels;

               result.ResultState = ValidationController.ValidateGetArchiveFilesModel(result.Result, globals, nameof(Models.GetArchiveFilesItemsModel.ConfigsToReferenceList));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {

        }
    }
}
