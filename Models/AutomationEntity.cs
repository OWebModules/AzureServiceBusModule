﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class AutomationEntity
    {
        public string ParameterNamespace { get; set; }
        public string JobJson { get; set; }
    }
}
