﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class ReceiveJobMessageModel
    {
        public clsOntologyItem Config { get; set; }

        public clsObjectRel ConfigToConnectionConfig { get; set; }

        public clsObjectRel ConnectionConfigToConnectionString { get; set; }

        public clsObjectAtt ConnectionString { get; set; }

        public clsObjectRel ConnectionConfigToQueuesTopicsRelay { get; set; }

    }
}
