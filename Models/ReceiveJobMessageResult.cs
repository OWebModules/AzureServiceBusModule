﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class ReceiveJobMessageResult
    {
        public long CountReceivedMessages { get; set; }
    }
}
