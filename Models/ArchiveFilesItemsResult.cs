﻿using MediaViewerModule.Models;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class ArchiveFilesItemsResult
    {
        public List<ResultItem<MultiMediaItem>> ArchivedMediaItems { get; set; }
    }
}
