﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class SendJobMessageResult
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem Config { get; set; }
        public string Json { get; set; }
    }
}
