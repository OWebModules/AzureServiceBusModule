﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class SendJobMessageModel
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ConfigsToConnectionConfigs { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConnectionConfigsToConnectionStrings { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> ConnectionStrings { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ConfigsToSyncStartModuleConfigs { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConnectionConfigToQueuesTopicsRelays { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> StartModuleConfigsJsons { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> StartModuleConfigsParameterNamespaces { get; set; } = new List<clsObjectAtt>();
    }
}
