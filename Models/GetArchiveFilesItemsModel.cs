﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class GetArchiveFilesItemsModel
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; }

        public List<clsObjectRel> ConfigsToAccountConfig { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToMediaTypes { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToReferenceList { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToExportFolder { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> AccountConfigsToConnectionStrings { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> AccountConfigsToUrls { get; set; } = new List<clsObjectRel>();

        
    }
}
