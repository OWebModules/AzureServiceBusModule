﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureServiceBusModule.Models
{
    public class ArchiveFilesItemsRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput {get; set;}

        public CancellationToken CancellationToken { get; private set; }

        public ArchiveFilesItemsRequest(string idConfig, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            CancellationToken = cancellationToken;
        }
    }
}
